from __future__ import annotations

from botpy import Utils, Args, InlineArticle, Bot, Button, Keyboard
from botpy import sanitize_html as s
from bggthebot import main
from bggthebot.boardgame import Boardgame
from bggthebot.collection import Collection
from bggthebot.db import init_db

import time
import threading


utils = Utils()
init_db()


COLLECTION_NAME_RE = r"[A-Za-z_\-\d]{4,20}"


start = (
    "Ciao!\n"
    "Grazie a me potrai cercare qualsiasi gioco da tavolo ed avere le relative"
    " informazioni prese direttamente dal sito BoardGameGeek!\n"
    "\n"
    "Per usarmi scrivi in qualsiasi chat (anche questa) @bggthebot seguito dal"
    " gioco che stai cercando, poi dammi un attimo per cercare il gioco e ti"
    " listerò i primi risultati che trovo. Infine clicca su uno dei risultati"
    " per inviare un messaggio con tutte le informazioni a riguardo e pure un"
    " link al gioco sul sito di boardgamegeek :)\n"
    "\n"
    "Scrivi /esempio per avere un video che mostra come utilizzarmi."
)

utils.start_message(start)


def send_example(bot: Bot, _):
    bot.telegram_bot.send_document(bot.chat_id, "BAACAgQAAxkBAAIPpGRIRa0XMyhWVaigAsyeSycriqeiAAL4DgACHbZAUmcax-OTyPg0LwQ")

utils.add_command("esempio", send_example)


def search_games(_, args: Args):
    search_query = args["game"]
    boardgames: list[Boardgame] = main.search_game_on_bgg(search_query)

    if not boardgames:
        return [InlineArticle(
            "Nessun risultato",
            "Non esiste un gioco con quel nome",
            f"La ricerca '{s(search_query)}' non ha prodotto nessun risultato",
            None
        )]

    articles = []
    for boardgame in boardgames:
        articles.append(InlineArticle(
            f"{boardgame.name} ({boardgame.year})",
            boardgame.to_message(),
            parse_mode="HTML",
            description=boardgame.description[:100],
            thumb_url=boardgame.thumbnail_url
        ))

    return articles

utils.add_inline_handler(
    "(.+)",
    search_games,
    arguments_names=["game"],
)


def close_keyboard(bot: Bot, _: Button):
    bot.edit_message(keyboard=None)

utils.add_button_handler(close_keyboard)


def create_collection(bot: Bot, button: Button):
    collection_name = button.params["collection_name"]

    exists = main.create_collection(collection_name, bot.user_id)

    bot.edit_message(keyboard=None)

    if exists:
        bot.reply(f"La collection '{collection_name}' esiste già")
    else:
        bot.reply(f"Collection '{collection_name}' creata")

utils.add_button_handler(create_collection)


# TODO actually download games when using "download"
# remember that collection may be very big (e.g. earendil)

def get_games_in_collection(bot: Bot, args: Args):
    collection_name = args.get("collection")

    # retrieve collection
    if not collection_name:
        collection_id = str(bot.user_id)
    else:
        maybe_collection: Collection | None = main.get_collection_with_name(collection_name, bot.user_id)
        if maybe_collection:
            collection_id = maybe_collection.id
        else:
            keyboard = Keyboard(
                [[
                    Button("Sì", create_collection, params={"collection_name": collection_name}),
                    Button("No", close_keyboard)
                ]]
            )
            bot.reply("Non hai nessuna collection con quel nome. Vuoi crearla?", keyboard=keyboard)
            return

    # retrieve games
    boardgames: list[Boardgame] = main.get_games_in_collection(collection_id)
    if not boardgames:
        bot.reply("Non c'è alcun gioco nella collection")
    else:
        first_boardgames = sorted(boardgames, key=lambda boardgame: boardgame.name)[:10]
        first_boardgames_str = "\n".join(game.to_short() for game in first_boardgames)

        bot.reply(
            f"Ci sono {len(boardgames)} nella collection.\n"
            "Eccone alcuni:\n"
            "\n"
            f"{first_boardgames_str}"
        )

collection_usage = (
    "Per visualizzare i giochi nella tua collection personale usare il comando\n\n"
    "/collection\n\n"
    "per visualizzare i giochi in un'altra collection o per creare un'altra collection usa il comando\n\n"
    "/collection <nome-collection>\n\n"
    "Il nome di una collection deve avere fra i 4 ed i 20 caratteri e può contenere solo lettere, numeri, _ e -"
)
utils.add_command("collection", get_games_in_collection, arguments={"collection": f"({COLLECTION_NAME_RE})?"}, usage=collection_usage)


def add_games_to_collection(bot: Bot, button: Button):
    params: dict = button.params
    collection_id: str = params.get("collection_id")

    if collection_id:
        collection: Collection = main.add_games_to_collection(params["game_ids"], collection_id, bot.user_id)
        collection_name = "personale" if collection.personal else f"'{collection.name}'"
        bot.edit_message(keyboard=None)
        bot.reply(f"Giochi di '{params['bgg_username']}' aggiunti alla collection {collection_name}")

        return

    collections: list[Collection] = main.retrieve_all_collections(bot.user_id)
    if not collections:
        bot.edit_message(keyboard=None)
        bot.reply(
            "Mi dispiace ma non possiedi altre collection\n"
            "Puoi crearne una con il comando /collection <nome-collection>"
        )
        return

    keyboard = Keyboard()
    for collection in collections:
        new_params = params.copy()
        new_params["collection_id"] = collection.id

        keyboard.add_row(Button(
            collection.name,
            add_games_to_collection,
            params=new_params
        ))

    bot.edit_message(keyboard=None)
    bot.reply("A quale collection vuoi aggiungere i giochi?", keyboard=keyboard)


utils.add_button_handler(add_games_to_collection)
def download_collection(bot: Bot, args: Args):
    bgg_username = args["bgg_username"]

    def successful_download(result):
        if len(result) == 0:
            bot.reply(
                f"La collection dell'utente bgg '{bgg_username}' non contiene alcun gioco"
            )
            return

        game_ids, game_titles = result
        game_titles_str = "\n".join(game_titles)

        keyboard = Keyboard([
            [Button(
                "Aggiungi alla mia collection personale",
                add_games_to_collection,
                {"bgg_username": bgg_username, "game_ids": game_ids, "collection_id": str(bot.user_id)}
            )],
            [Button(
                "Aggiungi ad una collection",
                add_games_to_collection,
                {"bgg_username": bgg_username, "game_ids": game_ids}
            )]
        ])

        bot.reply(
            f"Collection dell'utente bgg '{bgg_username}' scaricata.\n"
            f"Trovati {len(game_ids)} giochi.\n"
            f"Questi gli ultimi 10 giochi modificati:\n"
            "\n"
            f"{game_titles_str}"
            "\n\n"
            "Cosa vuoi fare?",
            keyboard = keyboard
        )

    def worker_wait_for_collection():
        n_retries = 0
        while n_retries < 5:
            time.sleep(5)

            success, result = main.download_collection(bgg_username)

            if not success or result is None:
                n_retries += 1
            else:
                successful_download(result)
                return

        bot.reply("Mi dispiace ma non sono riuscito a scaricare la collection. Riprova più tardi")

    bgg_username = args["bgg_username"]
    success, result = main.download_collection(bgg_username)

    if not success:
        bot.reply(f"Mi dispiace, ma lo username '{bgg_username}' non esiste su bgg")
        return

    if result is None:
        t = threading.Thread(target=worker_wait_for_collection)
        t.start()

        bot.reply(
            f"Collection dell'utente bgg '{bgg_username}' trovata!\n"
            "Download in corso, ti avviserò non appena avrò finito"
        )
    else:
        successful_download(result)


download_collection_usage = (
    "Utilizzo: /download <username-su-bgg>\n\n"
    "Uno username bgg è lungo almeno 4 caratteri, al più 20,"
    " deve iniziare con una lettera e può contenere solo lettere, numeri e _"
)
utils.add_command(
    "download",
    download_collection,
    {"bgg_username": r"[a-zA-Z][a-zA-Z0-9_]{3,19}"},
    usage=download_collection_usage
)


def add_cumulative_game_wrapper(bot: Bot, button: Button):
    stop, message, keyboard = add_cumulative_game_to_collection(bot, button.params)

    if stop:
        return message

    bot.edit_message(message, keyboard=keyboard)


def add_cumulative_game_to_collection(bot: Bot, params: dict):
    selected_game_id = params["selected_game_id"]
    collection_id = params["collection_id"]
    collection_name = params["collection_name"]
    remaining_games = params["remaining_games"]
    user_id = params["user_id"]

    if bot.user_id != user_id:
        return True, "Non hai richiesto tu questo comando", None

    if selected_game_id:
        main.add_games_to_collection([selected_game_id], collection_id, user_id)

    if not remaining_games:
        bot.edit_message(keyboard=None)
        bot.reply("Giochi aggiunti alla collection")
        return True, "Finito", None

    # add toast in the middle
    if bot.query:
        bot.context.bot.answer_callback_query(bot.query.id, "Sto cercando il prossimo gioco")

    next_game_name = remaining_games[0]

    boardgames: list[Boardgame] = main.search_game_on_bgg(next_game_name)

    button_params = {
        "selected_game_id": None,
        "remaining_games": remaining_games[1:],
        "collection_id": collection_id,
        "collection_name": collection_name,
        "user_id": bot.user_id
    }

    keyboard = Keyboard()
    for boardgame in boardgames:
        copy_params = button_params.copy()
        copy_params["selected_game_id"] = boardgame.id
        keyboard.add_row(
            Button(boardgame.to_short(sanitize=False), add_cumulative_game_wrapper, copy_params)
        )

    # skip and stop buttons
    skip_button = Button("Salta ➡️", add_cumulative_game_wrapper, button_params)
    copy_params = button_params.copy()
    copy_params["remaining_games"] = None
    stop_button = Button("Chiudi ❌", add_cumulative_game_wrapper, copy_params)
    keyboard.add_row([skip_button, stop_button])

    boardgame_names = "\n".join(boardgame.to_short(sanitize=False) for boardgame in boardgames)
    message = (
        f"Risultato della ricerca: '{next_game_name}'\n"
        "\n"
        f"Seleziona il gioco che vuoi inserire nella collection '{collection_name}'\n"
        "\n"
        f"{boardgame_names}"
    )

    return False, message, keyboard


utils.add_button_handler(add_cumulative_game_wrapper)


def add_game_to_collection(bot: Bot, args: Args):
    collection_name = args["collection_name"]
    game_names = args["game_names"]

    # first check if collection exists
    collection: Collection | None = main.get_collection_with_name(collection_name, bot.user_id)
    if not collection:
        bot.reply(f"Non possiedi alcuna collection chiamata '{collection_name}'")
        return

    game_list = game_names.split("\n")

    _, message, keyboard = add_cumulative_game_to_collection(
        bot, params={
            "selected_game_id": None,
            "collection_id": collection.id,
            "collection_name": collection.name,
            "remaining_games": game_list,
            "user_id": bot.user_id,
        }
    )

    bot.reply(message, keyboard=keyboard)


add_usage = (
    "Per aggiungere molteplici giochi all'interno di una collection usa il comando:\n"
    "\n"
    "/add <nome-collection>\n"
    "\n"
    "seguito da una lista di nomi che userò per cercare i giochi online.\n"
    "\n"
    "Ad esempio:\n"
    "\n"
    "/add giochi preferiti\n"
    "terra mystica\n"
    "carcass\n"
    "catan\n"
    "\n"
    "Non preoccuparti se non conosci il nome per intero, ti aiuterò io con la ricerca, basta che mi dai parte del nome"
)
# TODO add ability to add games to your personal collection
utils.add_command(
    "add",
    add_game_to_collection,
    {"collection_name": COLLECTION_NAME_RE, "game_names": "ANY"},
    add_usage
)


# TODO Add command to visualize all the collections

def search_game(bot: Bot, args: Args):
    collection_name = args["collection_name"]

    collection: Collection | None = main.get_collection_with_name(collection_name, bot.user_id)
    if not collection:
        bot.reply("Non hai nessuna collection con quel nome.")
        return

    boardgames: list[Boardgame] = main.filter_games_in_collection(
        collection_id = collection.id,
        players = int(args["players"]),
        from_minutes = int(args["from_minutes"]),
        to_minutes = int(args["to_minutes"]),
        max_weight = float(args["max_weight"]),
    )

    if not boardgames:
        bot.reply(f"Non ho trovato nessun gioco nella collection '{collection_name}' che corrisponde ai tuoi filtri")
        return

    games_str = "\n".join(boardgame.to_medium(sanitize=False) for boardgame in boardgames)
    bot.reply(
        f"I seguenti giochi nella collection '{collection_name}' corrispondono ai tuoi filtri:\n"
        "\n"
        f"{games_str}"
    )

# TODO improve HTML in messages among the whole repo

utils.add_command(
    "cerca",
    search_game,
    {
        "collection_name": COLLECTION_NAME_RE,
        "players": r"[1-9][0-9]?",
        "from_minutes": r"\d+",
        "to_minutes": r"\d+",
        "max_weight": r"([0-9]*[.])?[0-9]+"
    },
    usage = "/cerca <collection> <giocatori> <minuti-minimo> <minuti-massimo> <peso-massimo>"  # TODO improve usage
)


utils.start()
