from __future__ import annotations

import re

from dataclasses import dataclass
from bs4 import BeautifulSoup
from botpy import sanitize_html as s
from psycopg2.extras import DictRow


BR_RE = re.compile(r"<br/?>")
NO_IMAGE_AVAILABLE = "https://cf.geekdo-images.com/zxVVmggfpHJpmnJY9j-k1w__itemrep/img/Py7CTY0tSBSwKQ0sgVjRFfsVUZU=/fit-in/246x300/filters:strip_icc()/pic1657689.jpg"


@dataclass(init=True, repr=True)
class Boardgame:
    id: int
    name: str
    year: int
    rating: float
    position: None | int
    min_players: int
    max_players: int
    min_playing_time: int
    max_playing_time: int
    weight: float
    categories: list[str]
    mechanics: list[str]
    description: str
    thumbnail_url: str

    @classmethod
    def parse_api_game(cls, game):
        names = [name for name in game["name"] if name.get("@primary", "false") == "true"]
        ranks = game["statistics"]["ratings"]["ranks"]["rank"]
        global_ranking = [rank for rank in ranks if rank["@id"] == 1]
        description = BR_RE.sub("\n", game["description"])
        description = BeautifulSoup(description, features="html.parser")
        thumbnail = game["thumbnail"] if "thumbnail" in game else NO_IMAGE_AVAILABLE

        return cls(
            id = int(game["@objectid"]),
            name = names[0]["#text"],
            year = int(game["yearpublished"]),
            rating = float(game["statistics"]["ratings"]["average"]),
            position = global_ranking[0]["@value"] if global_ranking else None,
            min_players = int(game["minplayers"]),
            max_players = int(game["maxplayers"]),
            min_playing_time = int(game["minplaytime"]),
            max_playing_time = int(game["maxplaytime"]),
            weight = float(game["statistics"]["ratings"]["averageweight"]),
            categories = [el["#text"] for el in game.get("boardgamecategory", [])],
            mechanics = [el["#text"] for el in game.get("boardgamemechanic", [])],
            description = description.text,
            thumbnail_url = thumbnail,
        )

    @classmethod
    def parse_db_result(cls, game: DictRow):
        d = dict(game)
        del d["last_update"]

        return cls(**d)

    def to_message(self):
        if self.min_playing_time < self.max_playing_time:
            playing_time = f"{self.min_playing_time}-{self.max_playing_time}"
        else:
            playing_time = f"{max(self.min_playing_time, self.max_playing_time)}"

        position = f"{self.position}" if self.position else "-"
        year = self.year or "?"

        return (
            f"<b>{s(self.name)}</b> ({year})\n"
            f"<b>Posizione</b>: {position} (⭐️ {self.rating})\n"
            f"<b>Giocatori</b>: {self.min_players}-{self.max_players}\n"
            f"<b>Tempo di gioco</b>: {playing_time} minuti\n"
            f"<b>Peso</b>: {self.weight}\n"
            f"<b>Categorie</b>: {s(', '.join(self.categories))}\n"
            f"<b>Meccaniche</b>: {s(', '.join(self.mechanics))}\n"
            "\n"
            f"{self.description[:250]}... (<a href='https://boardgamegeek.com/boardgame/{self.id}'>link al gioco</a>)"
        )

    def to_short(self, sanitize=True):
        name = s(self.name) if sanitize else self.name
        return f"{name} ({self.year})"

    def to_medium(self, sanitize=True):
        name = s(self.name) if sanitize else self.name
        return f"{name} ({self.year})\n    👤: {self.min_players}-{self.max_players} ⌛️: {self.min_playing_time}-{self.max_playing_time} 🤔: {self.weight}"
