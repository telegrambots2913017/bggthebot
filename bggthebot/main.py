from __future__ import annotations

import requests
import xmltodict
from urllib import parse
from bs4 import BeautifulSoup
from pathlib import Path
from bggthebot.boardgame import Boardgame
from bggthebot.collection import Collection
from bggthebot import db
from botpy.log import logger


ALWAYS_LIST_FIELDS = set([
    "name",
    "boardgame",
    "rank",
    "boardgamedesigner",
    "boardgamecategory",
    "boardgamemechanic",
])


def search_game_on_bgg(name: str) -> list[Boardgame]:
    safe_name = parse.quote(name)
    res = requests.get(f"https://boardgamegeek.com/search/boardgame?sort=numvoters&q={safe_name}", timeout=10)
    res.raise_for_status()

    # TODO handle requests errors

    soup = BeautifulSoup(res.text, features="html.parser")
    links = soup.find("table", {"class": "collection_table"}).find_all("a", {"class": "primary"})
    game_ids = [Path(link.get("href")).parts[2] for link in links][:10]

    if not game_ids:
        return []

    return get_games_info_from_bgg(game_ids)


def get_games_info_from_bgg(game_ids: list[str]):
    # first search on local db
    boardgames: list[Boardgame] = db.get_games_with_ids(game_ids)
    retrieved_ids = [str(boardgame.id) for boardgame in boardgames]

    logger.debug(f"Retrieved {len(retrieved_ids)} boardgames from db")

    remaining_ids = [id for id in game_ids if id not in retrieved_ids]

    if remaining_ids:
        games_info = get_info_from_bgg_api(f"https://api.geekdo.com/xmlapi/boardgame/{','.join(remaining_ids)}?stats=1")

        api_boardgames = []

        for game in games_info:
            boardgame = Boardgame.parse_api_game(game)
            api_boardgames.append(boardgame)

        # update db with new boardgames
        db.insert_games(api_boardgames)

        boardgames += api_boardgames

    # order results based on how they were initially
    boardgames = sorted(boardgames, key=lambda x: game_ids.index(str(x.id)))

    return boardgames


def get_info_from_bgg_api(url):
    res = requests.get(url, timeout=10)
    res.raise_for_status()

    # TODO handle failed requests

    xml = res.text
    json = xmltodict.parse(xml, force_list=ALWAYS_LIST_FIELDS)

    return json["boardgames"]["boardgame"]


def get_games_in_collection(collection_id: str):
    return db.get_games_in_collection(collection_id)


def download_collection(bgg_username: str):
    res = requests.get(f"https://api.geekdo.com/xmlapi/collection/{bgg_username}?own=1", timeout=10)
    res.raise_for_status()

    json = xmltodict.parse(res.text, force_list=ALWAYS_LIST_FIELDS)
    if "errors" in json:
        if json["errors"]["error"]["message"] == "Invalid username specified":
            return False, None

    if "message" in json and "has been accepted" in json["message"]:
        # collection in downloading
        return True, None

    if json["items"]["@totalitems"] == "0":
        # empty collection
        return True, []

    games = json["items"]["item"]

    partial_boardgames = [
        (game["@objectid"], game["name"][0]["#text"], game.get("yearpublished", None))
        for game in games
    ]
    db.insert_partial_games(partial_boardgames)

    all_ids = [game["@objectid"] for game in games]
    last_10_modified = sorted(games, key=lambda game: game["status"]["@lastmodified"], reverse=True)[:10]

    last_10_titles = [
        f"{game['name'][0]['#text']} ({game['yearpublished']})"
        for game in last_10_modified
    ]

    return True, (all_ids, last_10_titles)


def add_games_to_collection(game_ids: list[str], collection_id: str, user_id: int):
    if str(user_id) == collection_id:
        db.create_collection(user_id, None, personal=True)

    collection = db.add_games_to_collection(collection_id, game_ids)
    return Collection.from_db(collection)


def retrieve_all_collections(user_id: int) -> list[Collection]:
    collections = db.retrieve_all_collections(user_id)
    return [Collection.from_db(collection) for collection in collections]


def get_collection_with_name(collection_name: str, user_id: int) -> Collection | None:
    collection = db.retrieve_collections_by_name(collection_name, user_id)
    if collection:
        return Collection.from_db(collection)
    return None


def create_collection(collection_name: str, user_id: int) -> bool:
    exists = db.create_collection(user_id, collection_name, personal=False)
    return exists


def filter_games_in_collection(collection_id: int, players: int, from_minutes: int, to_minutes: int, max_weight: float) -> list[Boardgame]:
    games = db.filter_games_in_collection(collection_id, players, from_minutes, to_minutes, max_weight)

    return [Boardgame.parse_db_result(game) for game in games]
