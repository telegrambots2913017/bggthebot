from dataclasses import dataclass

@dataclass
class Collection:
    id: str
    owner_user_id: int
    name: str
    personal: bool

    @classmethod
    def from_db(cls, collection: dict):
        return cls(**collection)
