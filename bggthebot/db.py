from __future__ import annotations

import json
import psycopg2
from psycopg2.extras import DictCursor
from botpy import config
from bggthebot.boardgame import Boardgame



cur = None

def init_db():
    conn = psycopg2.connect(
        host=config.database.host,
        user=config.database.user,
        password=config.database.password,
        dbname=config.database.name
    )
    conn.autocommit = True

    global cur  # TODO improve
    cur = conn.cursor(cursor_factory=DictCursor)


    cur.execute("CREATE SCHEMA IF NOT EXISTS bggthebot")

    cur.execute(
        """
            CREATE TABLE IF NOT EXISTS bggthebot.boardgame (
                id                  BIGINT        NOT NULL,
                name                TEXT          NOT NULL,
                year                INTEGER                    DEFAULT NULL,
                rating              DECIMAL                    DEFAULT NULL,
                position            INTEGER                    DEFAULT NULL,
                min_players         INTEGER                    DEFAULT NULL,
                max_players         INTEGER                    DEFAULT NULL,
                min_playing_time    INTEGER                    DEFAULT NULL,
                max_playing_time    INTEGER                    DEFAULT NULL,
                weight              DECIMAL                    DEFAULT NULL,
                categories          JSONB                      DEFAULT '[]'::JSONB,
                mechanics           JSONB                      DEFAULT '[]'::JSONB,
                description         TEXT                       DEFAULT NULL,
                thumbnail_url       TEXT                       DEFAULT NULL,
                last_update         TIMESTAMPTZ   NOT NULL     DEFAULT CURRENT_TIMESTAMP,

                PRIMARY KEY (id)
            );

            CREATE TABLE IF NOT EXISTS bggthebot.collection (
                id              TEXT        NOT NULL    DEFAULT gen_random_uuid(),
                owner_user_id   BIGINT      NOT NULL,
                name            TEXT        NOT NULL,
                personal        BOOLEAN     NOT NULL    DEFAULT FALSE,

                PRIMARY KEY (id),
                UNIQUE (owner_user_id, name)
            );
            CREATE INDEX IF NOT EXISTS collection_owner_user_id_idx ON bggthebot.collection (owner_user_id);

            CREATE TABLE IF NOT EXISTS bggthebot.collection_boardgame_m2m (
                collection_id   TEXT        NOT NULL,
                boardgame_id    BIGINT      NOT NULL,

                PRIMARY KEY (collection_id, boardgame_id),
                CONSTRAINT fk_collection_boardgame_m2m_collection FOREIGN KEY (collection_id) REFERENCES bggthebot.collection (id),
                CONSTRAINT fk_collection_boardgame_m2m_boardgame FOREIGN KEY (boardgame_id) REFERENCES bggthebot.boardgame (id)
            );

            CREATE TABLE IF NOT EXISTS bggthebot.collection_user_m2m (
                collection_id   TEXT        NOT NULL,
                user_id         BIGINT      NOT NULL,

                PRIMARY KEY (collection_id, user_id),
                CONSTRAINT fk_collection_user_m2m_collection FOREIGN KEY (collection_id) REFERENCES bggthebot.collection (id)
            );
        """
    )


def get_games_with_ids(ids: list[str]) -> list[Boardgame]:
    cur.execute("""
        SELECT * FROM bggthebot.boardgame
        WHERE
            id in %s AND
            description IS NOT NULL AND
            last_update > current_date - interval '1' day
    """, (tuple(ids),))
    res = cur.fetchall()

    boardgames = [Boardgame.parse_db_result(game) for game in res]

    return boardgames


def insert_games(boardgames: list[Boardgame]):
    values = []
    for boardgame in boardgames:
        value = (
            boardgame.id,
            boardgame.name,
            boardgame.year,
            boardgame.rating,
            boardgame.position,
            boardgame.min_players,
            boardgame.max_players,
            boardgame.min_playing_time,
            boardgame.max_playing_time,
            boardgame.weight,
            json.dumps(boardgame.categories),
            json.dumps(boardgame.mechanics),
            boardgame.description,
            boardgame.thumbnail_url,
        )
        values.append(value)

    args = ",".join(cur.mogrify("(" + "%s,"*13 + "%s)", value).decode("utf-8") for value in values)
    cur.execute(f"""
        INSERT INTO bggthebot.boardgame
        VALUES {args}
        ON CONFLICT (id)
            DO UPDATE SET
                name = excluded.name,
                year = excluded.year,
                rating = excluded.rating,
                position = excluded.position,
                min_players = excluded.min_players,
                max_players = excluded.max_players,
                min_playing_time = excluded.min_playing_time,
                max_playing_time = excluded.max_playing_time,
                weight = excluded.weight,
                categories = excluded.categories,
                mechanics = excluded.mechanics,
                description = excluded.description,
                thumbnail_url = excluded.thumbnail_url,
                last_update = CURRENT_TIMESTAMP
    """)


def insert_partial_games(games: list[(int, str, int)]):
    args = ",".join(cur.mogrify("(%s, %s, %s)", game).decode("utf-8") for game in games)
    cur.execute("INSERT INTO bggthebot.boardgame (id, name, year) VALUES " + args + "ON CONFLICT (id) DO NOTHING")


def get_games_in_collection(collection_id: str) -> list[Boardgame]:
    cur.execute("""
        SELECT * FROM bggthebot.boardgame WHERE id IN (
            SELECT boardgame_id FROM bggthebot.collection_boardgame_m2m WHERE collection_id = %s
        )
    """, (collection_id,))
    res = cur.fetchall()

    boardgames = [Boardgame.parse_db_result(game) for game in res]

    return boardgames


def create_collection(user_id: int, name: str = None, personal: bool = False) -> bool:
    if personal:
        cur.execute(
            "INSERT INTO bggthebot.collection VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING",
            (str(user_id), user_id, "PERSONAL", True)
        )
    else:
        cur.execute(
            "INSERT INTO bggthebot.collection (owner_user_id, name) VALUES (%s, %s) ON CONFLICT DO NOTHING",
            (user_id, name)
        )

    already_existed = (cur.rowcount == 0)
    return already_existed


def add_games_to_collection(collection_id: str, game_ids: list[str]) -> dict:
    args = ",".join(cur.mogrify("(%s, %s)", (collection_id, game_id)).decode("utf-8") for game_id in game_ids)

    cur.execute("INSERT INTO bggthebot.collection_boardgame_m2m VALUES" + args + "ON CONFLICT DO NOTHING")
    cur.execute("SELECT * FROM bggthebot.collection WHERE id = %s", (collection_id,))
    return cur.fetchone()


def retrieve_all_collections(user_id: int):
    cur.execute("SELECT * FROM bggthebot.collection WHERE owner_user_id = %s AND personal = false", (user_id,))
    return cur.fetchall()


def retrieve_collections_by_name(collection_name: str, user_id: int):
    cur.execute("SELECT * FROM bggthebot.collection WHERE owner_user_id = %s AND name = %s", (user_id, collection_name))
    return cur.fetchone()


def filter_games_in_collection(collection_id: int, players: int, from_minutes: int, to_minutes: int, max_weight: float):
    cur.execute("""
        SELECT * FROM bggthebot.boardgame WHERE id IN (
            SELECT boardgame_id FROM bggthebot.collection_boardgame_m2m WHERE collection_id = %s
        ) AND min_players <= %s AND max_players >= %s
            AND min_playing_time <= %s AND max_playing_time >= %s
            AND weight <= %s
    """, (collection_id, players, players, to_minutes, from_minutes, max_weight))

    return cur.fetchall()
